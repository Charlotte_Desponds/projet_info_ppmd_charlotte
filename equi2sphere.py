# Image Projection onto Sphere
# https://en.wikipedia.org/wiki/Equirectangular_projection
# Download the test image from the Wikipedia page!
# FB36 - 20160731
import numpy as np
from PIL import Image


def get_concat(im1, im2):
    """
    Agrège deux images en une.

    Parameters
    ----------
    im1 : PIL.IMAGE
        Première image.
    im2 : PIL.IMAGE
        Deuxième image.

    Returns
    -------
    new_img : PIL.IMAGE
        Nouvelle image.

    """
    new_img = Image.new('RGB', (im1.width + im2.width, im1.width + im2.width))
    new_img.paste(im1, (0, 0))
    new_img.paste(im2, (im1.width, 0))
    return new_img


def equi2Fisheye(Xe, Ye, We, He, FOV):
    """
    Permet de récupérer les coordonnées du repère équi dans le repère d'un fisheye'

    Parameters
    ----------
    Xe : Int
        Coordonnée X dans le repère equi.
    Ye : Int
        Coordonnée Y dans le repère equi.
    We : Int
        Largeur de l'image.
    He : Int
        Longueur de l'image.
    FOV : Float
        Ouverture de la caméra.

    Returns
    -------
    x : float
        Coordonnée x dans le repère fisheye.
    y : float
        Coordonnée x dans le repère fisheye.

    """
    # Calcul des angles des coordonnées dans un ref équivalent géographique
    theta = np.pi * (Xe / We - 0.5)
    phi = np.pi * (Ye / He - 0.5)

    # Calcul des valeurs x,y,z dans le ref géo
    px = np.cos(phi) * np.sin(theta)
    py = np.cos(phi) * np.cos(theta)
    pz = np.sin(phi)

    # Calcul angles des coordonnées dans ref sphérique
    theta = np.arctan2(pz, px)
    phi = np.arctan2(np.sqrt(px ** 2 + pz ** 2), py)
    r = We * phi / FOV

    # Calcul des coordonnées normalisées du fisheye
    # Dénormalise et décentre
    x = (0.5 * We + r * np.cos(theta))
    y = (0.5 * He + r * np.sin(theta))

    return x, y


def fisheye2Equi(Xf, Yf, Wf, Hf, FOV):
    """
    Permet de récupérer les coordonnées du repère fisheye dans le repère équi.

    Parameters
    ----------
    Xf : Int
        Coordonnée X dans le repère fisheye.
    Yf : Int
        Coordonnée Y dans le repère fisheye.
    Wf : Int
        Largeur de l'image.
    Hf : Int
        Longueur de l'image.
    FOV : Float
        Ouverture de la caméra.

    Returns
    -------
    Float
        Coordonnée x dans le repère equi. Vaut -1 si [Xf,Yf] est dans la zone noire
    Float
        Coordonnée y dans le repère equi. Vaut -1 si [Xf,Yf] est dans la zone noire

    """
    # Les coordonnées sont normées
    # L'origine est placé au centre de l'image
    # Les coordonnées sont entre -1 et 1
    x = 2 * (Xf / (Wf - 1) - 0.5)
    y = 2 * (Yf / (Hf - 1) - 0.5)

    # Calcul de la distance
    r = np.sqrt(x ** 2 + y ** 2)

    # Si la distance est supérieur à 1, c'est une zone noire
    if r > 1:
        return -1, -1
    else:
        # Calcul des angles
        phi = r * FOV / 2
        theta = np.arctan2(y, x)
        
        # Calcul du point vectoriel 3D
        px = np.sin(phi) * np.cos(theta)
        py = np.cos(phi)
        pz = np.sin(phi) * np.sin(theta)
        
        # Calcul longitude et latitude
        long = np.arctan2(py, px)

        lat = np.arctan2(pz, np.sqrt(px ** 2 + py ** 2))

        # Calcul des coordonnées dans le repère equirectangulaire
        # Décentre y (x est déjà décentrée)
        # Dénormalise
        x = (long / np.pi) * Wf
        y = (2 * lat / np.pi + 1) / 2 * Hf
        return x, y


def main(url_img_equi):
    """
    Récupère une image equirectangulaire (créé à partir de deux lentilles fisheye) et en déduit 2 images correspondant à la vue brute de chaque caméra

    Parameters
    ----------
    url_img_equi : string
        Url vers l'image en projection equirectangulaire.

    Returns
    -------
    None.

    """
    # Récupération des images
    img_equi = Image.open(url_img_equi)

    # Création d'une nouvelle image fisheye
    fisheyeImage = Image.new("RGB", img_equi.size)
    pixelsEqui = img_equi.load()
    pixelsSphere = fisheyeImage.load()

    # Initialisation de la taille des images
    W = img_equi.size[0];
    H = img_equi.size[1];
    # Initialisation de l'ouverture de chaque caméra
    FOV = (180 * np.pi) / 180;

    for Xf in range(W):
        for Yf in range(H):
            x, y = fisheye2Equi(Xf, Yf, W, H, FOV)
            pixelsSphere[Xf, Yf] = pixelsEqui[x, y]
    url_img_fisheye_out = 'fish' + url_img_equi
    fisheyeImage.transpose(Image.FLIP_LEFT_RIGHT).save(url_img_fisheye_out, "PNG")
    print('Creating...' + url_img_fisheye_out)


def main2(url_img_equi):
    """
    Récupère une image equirectangulaire (créé à partir de deux lentilles fisheye) et en déduit 2 images correspondant à la vue brute de chaque caméra

    Parameters
    ----------
    url_img_equi : string
        Url vers l'image en projection equirectangulaire.

    Returns
    -------
    None.

    """
    # Récupération des images
    img_equi = Image.open(url_img_equi)

    # Découpage de l'image (image de chaque lentilles)
    # Gauche
    # img_equi_crop_gauche = img_equi.crop((0,0,img_equi.size[0]/2,img_equi.size[1]))     
    img_equi_crop_gauche = img_equi.crop((img_equi.size[0] / 4, 0, 3 * img_equi.size[0] / 4, img_equi.size[1]))
    # Droite
    img_equi_crop_droite = get_concat(img_equi.crop((3 * img_equi.size[0] / 4, 0, img_equi.size[0], img_equi.size[1])),img_equi.crop((0, 0, img_equi.size[0] / 4, img_equi.size[1])))
    #Visualisation des images tronquées
    # img_equi_crop_gauche.save("crop_d.png", "PNG")

    # Création des nouvelles image fisheye
    # Gauche
    fisheyeImage_gauche = Image.new("RGB", img_equi_crop_gauche.size)
    pixelsEqui_gauche = img_equi_crop_gauche.load()
    pixelsSphere_gauche = fisheyeImage_gauche.load()
    # Droite
    fisheyeImage_droite = Image.new("RGB", img_equi_crop_droite.size)
    pixelsEqui_droite = img_equi_crop_droite.load()
    pixelsSphere_droite = fisheyeImage_droite.load()

    # Initialisation de la taille des images
    W = img_equi_crop_gauche.size[0];
    H = img_equi_crop_gauche.size[1];
    # Initialisation de l'ouverture de chaque caméra
    FOV = (190 * np.pi) / 180;

    ### On parcours l'image connu (equirectantgulaire) pour en déduire celle inconnu (fisheye)
    # for Xe in range (W):
    #     for Ye in range(H):
    #         x,y = equi2Fisheye(Xe, Ye, W, H, FOV)
    #         pixelsSphere_gauche[x,y] = pixelsEqui_gauche[Xe, Ye]
    #         pixelsSphere_droite[x,y] = pixelsEqui_droite[Xe, Ye]
    # fisheyeImage_gauche.save("sphere_g.png", "PNG")
    # fisheyeImage_droite.save("sphere_d.png", "PNG")

    # On parcours l'image à créer (fisheye) et on en déduit l'equivalent sur l'image connu (équirectangulaire)
    for Xf in range(W):
        for Yf in range(H):
            x, y = fisheye2Equi(Xf, Yf, W, H, FOV)
            pixelsSphere_gauche[Xf, Yf] = pixelsEqui_gauche[x, y]
            # pixelsSphere_droite[Xf, Yf] = pixelsEqui_droite[x, y]
    url_img_fisheye_out = 'fish' + url_img_equi
    fisheyeImage_gauche.save(url_img_fisheye_out + "_g.png", "PNG")
    print('Creating...' + url_img_fisheye_out + "_g.png")
    fisheyeImage_droite.save(url_img_fisheye_out + "_d.png", "PNG")
    print('Creating...' + url_img_fisheye_out + "_d.png")


if __name__ == "__main__":
    main("GS015077_droite0.png")
