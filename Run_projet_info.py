# -*- coding: utf-8 -*-
"""
Created on Mon Jan 10 11:16:16 2022

@author: 33658
# """
import subprocess
import cv2
import glob
import Lecture_video as lect
import equi2sphere as convert
import platform
import os



def commande(cmd):
    """
    Lance une ligne de commande

    Parameters
    ----------
    cmd : String
        Ligne de commande.

    Returns
    -------
    None.

    """
    print(cmd)
    p = subprocess.run(cmd, shell=True, capture_output=True, text=True)
    print(p.stdout)
    print(p.stderr)


def droite(d):
    """
    Parameters
    ----------
    d : bool
        True -> droite, False -> gauche
    Returns
    -------
    str.

    """
    return 'droite' if d else 'gauche'


def elague_video(nom_video, t_debut, duree):
    """
    Elague une vidéo

    Parameters
    ----------
    nom_video : String
        Lien vers la vidéo.
    t_debut : String de la forme 'hh:mm:ss.msms'
        Temps à partir duquel la vidéo élaguée commence.
    duree : String de la forme 'hh:mm:ss.msms'
        Durée de la vidéo élaguée.

    Returns
    -------
    None.

    """
    nom = nom_video.split('.')[0] + '_elague.mp4'
    cmd_vid_elague = 'ffmpeg -y -hide_banner -loglevel error -i ' + nom_video + ' -ss ' + t_debut + ' -t ' + duree + ' -c copy ' + nom
    commande(cmd_vid_elague)
    return nom


def separation2(video_source, equi):
    """
    Sépare en 2 la vidéo d'origine.
    Attention les valeurs de séparation sont en dur. Le format corresponds au image d'une GoPro MAX.

    Parameters
    ----------
    video_source : String
        Lien vers la vidéo originale.
    equi : Bool
        L'image est en projection équirectangulaire.

    Returns
    -------
    None.

    """
    nom_g = video_source.split('.')[0] + '_gauche.mp4'
    nom_d = video_source.split('.')[0] + '_droite.mp4'
    video = cv2.VideoCapture(video_source)
    height = video.get(cv2.CAP_PROP_FRAME_HEIGHT)
    width = video.get(cv2.CAP_PROP_FRAME_WIDTH)
    if equi:
        cmd_vid_g = 'ffmpeg -y -hide_banner -loglevel error -i ' + video_source + ' -filter:v "crop='+ str(int(width/2)) + ':' + str(int(height)) + ':' + str(int(width/4)) + ':0" -c:a copy ' + nom_g
        cmd_vid_d1 = 'ffmpeg -y -hide_banner -loglevel error -i ' + video_source + ' -filter:v "crop='+ str(int(width/4)) + ':' + str(int(height)) + ':0:0" -c:a copy d1.mp4'
        cmd_vid_d2 = 'ffmpeg -y -hide_banner -loglevel error -i ' + video_source + ' -filter:v "crop='+ str(int(width/4)) + ':' + str(int(height)) + ':' + str(int(width*3/4)) + ':0" -c:a copy d2.mp4'
        commande(cmd_vid_d1)
        commande(cmd_vid_d2)
        cmd_vid_d = 'ffmpeg -y -hide_banner -loglevel error -i d2.mp4 -i d1.mp4 -filter_complex hstack=inputs=2 -c:a copy ' + nom_d
    else:
        cmd_vid_g = 'ffmpeg -y -hide_banner -loglevel error -i ' + video_source + ' -filter:v "crop='+ str(int(width/2)) + ':' + str(int(height)) + ':0:0" -c:a copy ' + nom_g
        cmd_vid_d = 'ffmpeg -y -hide_banner -loglevel error -i ' + video_source + ' -filter:v "crop='+ str(int(width/2)) + ':' + str(int(height)) + ':'+ str(int(width/2)) + ':0" -c:a copy ' + nom_d
    commande(cmd_vid_g)
    commande(cmd_vid_d)
    if equi:
        if platform.system() == "Windows":
            commande('del d1.mp4')
            commande('del d2.mp4')
        else:
            commande('rm d1.mp4')
            commande('rm d2.mp4')
    return nom_g, nom_d


def DIV(nom_video, nb_frame):
    """
    Lance la commande DIV de MicMac. Récupère les images d'une vidéo.

    Parameters
    ----------
    nom_video : String
        Url de la vidéo.
    nb_frame : Int
        ...

    Returns
    -------
    None.

    """
    if platform.system() == "Windows":
        print("DIV n'est pas disponible sous Windows")
        lect.save_frame(nom_video, nb_frame)
    else:
        cmd_DIV = 'mm3d DIV ' + nom_video + ' -1'
        commande(cmd_DIV)
        commande('mkdir POUB')
        if platform.system() == "Windows":
            commande('move *Nl.png POUB')
        else:
            commande('mv *Nl.png POUB')


def SetExif(focale, focale_35):
    """
    Ajoute les paramètres EXIF au img.png

    Parameters
    ----------
    focale : float
        Focale de la caméra.
    focale_35 : float
        Focale équivalent 35mm.

    Returns
    -------
    None.

    """
    cmd_SetExif = 'mm3d SetExif ".*png" F=' + str(focale) + ' F35=' + str(focale_35)
    commande(cmd_SetExif)


def saisieMasq(img):
    """
    Création d'un masque et ajout de celui ci sur les images.png'

    Parameters
    ----------
    img : String
        Url de l'image pour initialiser le masque.
    d : Bool
        Vérifie si l'image vient de la caméra de droite.

    Returns
    -------
    None.

    """
    cmd_SaisieMasq = 'mm3d SaisieMasqQt ' + img
    commande(cmd_SaisieMasq)


def calib_ori(d):
    """
    Création d'un fichier de calibration pour une caméra fisheye'
    
    Parameters
    ----------
    d : Bool
        Vérifie si l'image vient de la caméra de droite.
    Returns
    -------
    None.

    """
    cmd_FilterMask = ""
    cmd_Tapioca = 'mm3d Tapioca Line ".*'+droite(d)+'.*png" 1500 4'
    for img in glob.glob('*'+droite(d)+'*.*png'):
        saisieMasq(img)
        if os.path.isfile(img.split('.')[0] + '_Masq.tif'):
            cmd_FilterMask = 'mm3d HomolFilterMasq ".*'+droite(d)+'.*png" GlobalMasq="' + img.split('.')[0] + '_Masq.tif"'
        break
    cmd_Tapas = 'mm3d Tapas HemiEqui ".*'+droite(d)+'.*png" PropDiag=0.65 Out=Reg_'+droite(d)+'_Regul RegulDist=[1,10,100]'
    # cmd_Tapas2 = 'mm3d Tapas HemiEqui ".*'+droite(d)+'.*png" PropDiag=0.65 InOri=Reg_'+droite(d)+'_Regul Out=Reg_'+droite(d)
    commande(cmd_Tapioca)
    if cmd_FilterMask != "":
        commande(cmd_FilterMask)
        if platform.system() == "Windows":
            commande('move Homol HomolInit')
            commande('move HomolMasqFiltered Homol')
        else:
            commande('mv Homol HomolInit')
            commande('mv HomolMasqFiltered Homol')
    commande(cmd_Tapas)
    # commande(cmd_Tapas2)

def calib_aspro(GCP_url):
    cmd_saisiePoint = 'mm3d SaisieAppuisInitQT ".*png" NONE '+GCP_url+' MeasureInit.xml'
    cmd_aspro='mm3d ".*png" Ori=Reg_d '+GCP_url + ' MeasureInit.xml'
    commande(cmd_saisiePoint)
    commande(cmd_aspro)


def main(nom_video, elague, equi, focale, focale_35):
    """
    Run

    Parameters
    ----------
    nom_video : String
        Url video.
    elague : Bool
        Elaguer la vidéo ?.
    equi : Bool
        L'image est en projection équirectangulaire.
    focale : float
        Focale de la caméra.
    focale_35 : float
        Focale 35 mm de la caméra.
    nb_frame : int
        Nb frame désiré.

    Returns
    -------
    None.

    """

    # On raccourcit la vidéo. Ici le t_début et la durée sont pré-définis
    if elague:
        nom_video = elague_video(nom_video, '00:00:03', '00:00:02')

    # On découpe la vidéo selon la projection de la vidéo en entrée
    vid_g, vid_d = separation2(nom_video, equi)
    
    # On récupère les images de la vidéo selon la méthode voulue
    DIV(vid_g, 15)
    DIV(vid_d, 15)
    # On convertit les images en dual fisheye si ce n'est pas déjà le cas
    if equi:
        for img in glob.glob('*.png'):
            convert.main(img)
            if platform.system() == "Windows":
                commande('move '+img+' POUB')
            else:
                commande('mv '+img+' POUB')

    # # GoPro MAX specs
    SetExif(focale, focale_35)

    # Launch Tapioca, SaisieMasq (and HomolFilterMasq if needed) and Tapas
    calib_ori(True)
    calib_ori(False)


def aide():
    print("************")
    print("**  Aide  **")
    print("************")
    print("Arguments requis :")
    print("\t* string :: {Path vers la vidéo}")
    print("\t* bool   :: {Découper une partie de la vidéo (début=00:00:03, durée=00:00:02)}")
    print("\t* bool   :: {Vidéo équirectangulaire ?}")
    print("\t* float  :: {Distance focale (mm)}")
    print("\t* float  :: {Distance focale équivalente 35 mm (mm)}")


### COMMENTER CELUI A NE PAS UTILISER ###

# if __name__ == '__main__':
#     if len(sys.argv) < 6 or any(arg == '-help' for arg in sys.argv):
#         aide()
#     else:
#         main(sys.argv[1:])


if __name__ == "__main__":
    nom_video = "GS015081.mp4"
    f = 8.9
    f35 = 49

    main(nom_video, elague=True, equi=True, focale=f, focale_35=f35)

