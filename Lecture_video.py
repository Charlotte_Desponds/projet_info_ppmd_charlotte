# -*- coding: utf-8 -*-
"""
Created on Mon Jan 10 11:00:58 2022

@author: 33658
"""
import cv2

def save_frame(nom_cam,nb_frame):
    """
    Enregistre dans un dossier data un nombre de frame voulu (au format .jpg) d'une vidéo.
    
    Parameters
    ----------
    nom_cam : String
        Url de la vidéo.
    nom_cam : String
        Nom des frames.
    nb_frame : INT
        Le nombre de frame (image) voulu

    Returns
    -------
    None.
    
    """
    cam = cv2.VideoCapture(nom_cam)
      
    current_frame = 0
    e=0
    while current_frame < nb_frame:
        ret,frame = cam.read() 
        if ret and e%3 ==0 : 
            name = nom_cam.split('.')[0] + str(current_frame) + '.png'
            print ('Creating...' + name) 
            cv2.imwrite(name, frame)  
            current_frame += 1 
        e+=1
            
        
    cam.release() 
    cv2.destroyAllWindows() 
    
if __name__ == "__main__":
    save_frame("GS015077.LRV",10)
